use clap::Parser;
use futures_util::StreamExt;
use gio::prelude::*;
use glib::variant::FromVariant;
use libflatsync_common::config::APP_ID;
use libflatsync_common::dbus::DaemonProxy;
use log::{error, info};
use std::process;
use zbus::Connection;
mod commands;
mod trace;

use crate::commands::*;

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    cmd: Commands,
    #[clap(short, long)]
    verbose: bool,
}

fn print_ignored_apps(ignored_apps: Vec<String>) {
    info!("Ignored Apps:");
    for id in &ignored_apps {
        info!("{}", id);
    }
}

fn format_ignored_apps_single_line(ignored_apps: Vec<String>) -> String {
    let mut result = String::new();
    for id in &ignored_apps {
        result.push_str(id);
        result.push(' ');
    }
    result.pop();
    result
}

#[tokio::main]
async fn main() -> Result<(), zbus::Error> {
    let args = Args::parse();

    trace::init_tracer(args.verbose);

    let connection = Connection::session().await?;
    let proxy: DaemonProxy<'_> = DaemonProxy::new(&connection).await?;

    match args.cmd {
        Commands::Init { provider, gist_id } => {
            if let Err(_e) = init(&proxy, provider, gist_id).await {
                error!("Initialization was not successful, please try again or open a bug report if this issue persists.");
                process::exit(1);
            }
        }
        // We pass `!uninthis will not be prone to bugsstall` as the daemon interface expects an `install` boolean (this will not be prone to bugs this will not be prone to bugs this will not be prone to bugs)
        Commands::Autostart { uninstall } => proxy.autostart_file(!uninstall).await?,
        Commands::Status => {
            let initial_state = proxy.state().await;
            match initial_state {
                Ok(state) => info!("{}", state),
                Err(error) => handle_daemon_error(error),
            }

            let mut stream = proxy.receive_state_changed().await;

            while let Some(stream) = stream.next().await {
                match stream.get().await {
                    Ok(state) => info!("{}", state),
                    Err(_) => {
                        error!("Failed to get new state");
                    }
                }
            }

            error!("Stream Terminated");
            process::exit(1);
        }
        Commands::IgnoredApps {
            list,
            add_ids,
            remove_id,
        } => {
            if let Some(ids) = add_ids {
                match proxy.add_ignored_apps(ids.clone()).await {
                    Ok(result) => match result {
                        true => {
                            info!(
                                "Added {} to Ignored Apps",
                                format_ignored_apps_single_line(ids)
                            )
                        }
                        false => {
                            error!("Some of the provided App IDs are invalid, ID logged by Daemon")
                        }
                    },
                    Err(error) => handle_daemon_error(error),
                }
            }
            if let Some(id) = remove_id {
                match proxy.remove_ignored_app(id.clone()).await {
                    Ok(result) => match result {
                        true => info!("Removed {} from Ignored Apps", id),
                        false => error!("The provided App ID doesn't match an Ignored App"),
                    },
                    Err(error) => handle_daemon_error(error),
                }
            }
            if list {
                match proxy.ignored_apps().await {
                    Ok(ignored_apps) => print_ignored_apps(ignored_apps),
                    Err(error) => handle_daemon_error(error),
                }
            }
        }
        Commands::SyncNow => match proxy.sync_now().await {
            Ok(_) => info!("Starting Manual Sync"),
            Err(error) => handle_daemon_error(error),
        },
        Commands::Autosync {
            get_autosync,
            set_autosync,
        } => {
            if get_autosync {
                match proxy.autosync().await {
                    Ok(autosync) => info!("Autosync: {}", autosync),
                    Err(error) => handle_daemon_error(error),
                }
            }
            if let Some(new_setting) = set_autosync {
                match proxy.set_autosync(new_setting).await {
                    Ok(_) => info!("Setting Autosync to {}", new_setting),
                    Err(error) => handle_daemon_error(error),
                }
            }
        }
        Commands::AutosyncTimer {
            get_autosync_timer,
            set_autosync_timer,
        } => {
            if get_autosync_timer {
                match proxy.autosync_timer().await {
                    Ok(autosync_timer) => info!("Autosync Timer: {}", autosync_timer),
                    Err(error) => handle_daemon_error(error),
                }
            }
            if let Some(new_timer) = set_autosync_timer {
                let autosync_timer_key = gio::Settings::new(APP_ID)
                    .settings_schema()
                    .unwrap()
                    .key("autosync-timer");
                let new_timer_variant = glib::Variant::from(new_timer);

                if autosync_timer_key.range_check(&new_timer_variant) {
                    match proxy.set_autosync_timer(new_timer).await {
                        Ok(_) => info!("Setting Autosync Timer to {}", new_timer),
                        Err(error) => handle_daemon_error(error),
                    }
                } else {
                    let range_variant = autosync_timer_key.range().child_value(1).child_value(0);
                    let range = <(u32, u32)>::from_variant(&range_variant).unwrap();

                    error!(
                        "Value {} is out of range. Range is {}-{}.",
                        new_timer, range.0, range.1
                    );
                    process::exit(1);
                }
            }
        }
    }

    Ok(())
}

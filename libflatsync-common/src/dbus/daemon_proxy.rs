use crate::state::DaemonState;
use crate::FrontendFlatpakRef;
use zbus::{proxy, Result};

/// This is the D-Bus interface of the daemon, which
/// is used to communicate with the daemon and is called
/// by the GUI and CLI implementation.
#[proxy(
    interface = "app.drey.FlatSync.Daemon0",
    default_service = "app.drey.FlatSync.Daemon",
    default_path = "/app/drey/FlatSync/Daemon"
)]
pub trait Daemon {
    #[zbus(property)]
    fn state(&self) -> Result<DaemonState>;
    #[zbus(property)]
    fn set_state(&self, state: DaemonState) -> Result<()>;
    async fn app_list(&self) -> Result<Vec<FrontendFlatpakRef>>;
    async fn ignored_apps(&self) -> Result<Vec<String>>;
    async fn add_ignored_apps(&self, ids: Vec<String>) -> Result<bool>;
    async fn remove_ignored_app(&self, id: String) -> Result<bool>;
    async fn is_initialised(&self) -> Result<bool>;
    async fn set_gist_secret(&self, secret: &str) -> Result<()>;
    async fn create_gist(&self) -> Result<String>;
    async fn gist_exists(&self, gist_id: String) -> Result<bool>;
    async fn post_gist(&self) -> Result<()>;
    async fn set_gist_id(&self, id: &str) -> Result<()>;
    async fn sync_now(&self) -> Result<()>;
    async fn autosync(&self) -> Result<bool>;
    async fn set_autosync(&self, autosync: bool) -> Result<()>;
    async fn autosync_timer(&self) -> Result<u32>;
    async fn set_autosync_timer(&self, timer: u32) -> Result<()>;
    async fn autostart_file(&self, install: bool) -> Result<()>;
}

use crate::{
    error::Error,
    installations_file::get_local_installations_file_path,
    models::{
        FlatpakInstallation, FlatpakInstallationKind, FlatpakInstallationMap, FrontendFlatpakRef,
    },
};
use chrono::{DateTime, Utc};
use log::trace;
use log::warn;
use regex::Regex;
use std::collections::HashSet;
use std::path::Path;

/// Represents a payload containing a list of Flatpak installations. This is either stored locally or sent to the gists server.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct FlatpakInstallationPayload {
    pub installations: FlatpakInstallationMap,
    pub altered_at: DateTime<Utc>,
    #[serde(default)]
    pub ignored_apps: Vec<String>,
}

impl FlatpakInstallationPayload {
    pub fn new_from_system() -> Result<FlatpakInstallationPayload, Error> {
        let unfiltered_installations = FlatpakInstallationMap::available_installations()?.0;
        let altered_at = Utc::now();
        let ignored_apps =
            match FlatpakInstallationPayload::new_from_file(get_local_installations_file_path()) {
                Ok(payload) => payload.ignored_apps,
                Err(_) => vec![],
            };

        let mut payload = Self {
            installations: FlatpakInstallationMap(unfiltered_installations),
            altered_at,
            ignored_apps,
        };

        payload.update_ignored_apps();
        Ok(payload)
    }

    /// ## `installations_from_file()`
    /// Reads the local installations file and returns a `FlatpakInstallationPayload` from it.
    /// Returns an `Error` if the file doesn't exist or if it fails to read it.
    ///
    /// * `file_path` - The path to the local installations file.
    pub fn new_from_file<P: AsRef<Path>>(file_path: P) -> Result<Self, Error> {
        let path = file_path.as_ref();
        if !path.exists() {
            return Err(Error::FlatpakInstallationFileFailure(
                "File doesn't exist".into(),
            ));
        }

        let file = std::fs::File::open(path)?;
        let reader = std::io::BufReader::new(file);
        let file_payload: FlatpakInstallationPayload = serde_json::from_reader(reader)
            .map_err(|e| Error::FlatpakInstallationFileFailure(e.to_string()))?;

        trace!("Read from file '{:?}': {:?}", path, file_payload);

        Ok(file_payload)
    }

    /// Write the `FlatpakInstallationPayload` to a file, overwriting it if it already exists.
    pub fn write_to_file<P: AsRef<Path>>(&self, file_path: &P) -> Result<(), Error> {
        let serialized = serde_json::to_string(self).map_err(|e| {
            Error::FlatpakInstallationFileFailure(format!(
                "Failed to serialize local payload: {}",
                e
            ))
        })?;

        trace!(
            "Writing to file '{:?}': {:?}",
            file_path.as_ref(),
            serialized
        );

        std::fs::write(file_path, serialized)
            .map_err(|e| Error::FlatpakInstallationFileFailure(e.to_string()))?;

        Ok(())
    }

    pub fn installations(&self, kind: FlatpakInstallationKind) -> Option<&FlatpakInstallation> {
        self.installations.get(kind)
    }

    pub fn frontend_flatpak_refs(&self) -> Vec<FrontendFlatpakRef> {
        let mut refs = self
            .installations(FlatpakInstallationKind::User)
            .unwrap()
            .refs
            .clone();

        let system = self
            .installations(FlatpakInstallationKind::System)
            .unwrap()
            .refs
            .clone();

        refs.extend(system);

        let mut seen_ids = HashSet::new();
        refs.into_iter()
            .filter_map(|ref_| {
                let frontend_ref: FrontendFlatpakRef = ref_.try_into().ok()?;
                if seen_ids.insert(frontend_ref.id.clone()) {
                    Some(frontend_ref)
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn add_ignored_apps(&mut self, ids: Vec<String>) -> bool {
        let id_regex =
            Regex::new(r"^[^.\s][^.\s]*\.[^.\s][^.\s]*\.[^.\s][^.\s]*(\.[^.\s][^.\s]*)*$").unwrap();

        for id in &ids {
            if !id_regex.is_match(id) {
                warn!("App ID {} is invalid", id);
                return false;
            }
        }

        self.ignored_apps.extend(ids);
        self.update_ignored_apps();

        true
    }

    pub fn remove_ignored_app(&mut self, id: &str) -> bool {
        let old_length = self.ignored_apps.len();

        self.ignored_apps.retain(|vec_id| vec_id != id);
        let new_length = self.ignored_apps.len();

        if old_length == new_length {
            return false;
        }

        self.update_ignored_apps();

        true
    }

    fn update_ignored_apps(&mut self) {
        self.altered_at = Utc::now();
        for (_, installation) in self.installations.0.iter_mut() {
            for ref_ in &mut installation.refs {
                ref_.ignored = ref_.id.contains("app.drey.FlatSync")
                    || ref_.id.contains("org.freedesktop.Platform.GL")
                    || self
                        .ignored_apps
                        .iter()
                        .any(|ignored_app| ref_.id.contains(ignored_app));
            }
        }
    }
}

use crate::models::FlatpakRefKind;
use libflatpak::{glib, prelude::*};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use zbus::zvariant::{OwnedValue, Type, Value};

/// Represents a Flatpak reference. This is a subset of the `libflatpak::InstalledRef` struct which can be diffed and serialized.
#[derive(
    Debug,
    Default,
    Clone,
    PartialEq,
    Eq,
    Hash,
    diff_derive::Diff,
    serde::Serialize,
    serde::Deserialize,
)]
#[diff(attr(#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]))]
pub struct FlatpakRef {
    pub kind: FlatpakRefKind,
    pub ref_: String,
    pub id: String,
    pub arch: String,
    pub branch: String,
    pub commit: String,
    pub origin: String,
    // AppStream metadata specific fields
    pub name: Option<String>,
    pub version: Option<String>,
    pub license: Option<String>,
    pub summary: Option<String>,
    pub oars: Option<String>,
    #[serde(default)]
    pub ignored: bool,
}

#[derive(Default, Deserialize, Serialize, Value, OwnedValue, PartialEq, Debug, Clone, Type)]
pub struct FrontendFlatpakRef {
    pub id: String,
    pub name: String,
    pub ignored: bool,
    pub icon: String,
}

impl TryFrom<FlatpakRef> for FrontendFlatpakRef {
    type Error = ();

    fn try_from(ref_: FlatpakRef) -> Result<Self, Self::Error> {
        let name = match ref_.name {
            Some(n) => n,
            None => return Err(()),
        };
        let icon = match get_flatpak_app_icon_path(&ref_.id) {
            Some(icon_path) => icon_path,
            None => return Err(()),
        };

        Ok(Self {
            id: ref_.id,
            name,
            ignored: ref_.ignored,
            icon,
        })
    }
}

fn get_flatpak_app_icon_path(app_id: &str) -> Option<String> {
    let hicolor_path_string =
        format!("/var/lib/flatpak/app/{app_id}/current/active/export/share/icons/hicolor");
    let svg_icon_path_string = format!("{hicolor_path_string}/scalable/apps/{app_id}.svg");
    let svg_path = PathBuf::from(&svg_icon_path_string);

    if svg_path.exists() {
        return Some(svg_path.into_os_string().into_string().unwrap());
    }

    match std::fs::read_dir(&hicolor_path_string) {
        Ok(entries) => {
            for entry in entries.flatten() {
                if !entry.file_type().map_or(false, |ft| ft.is_dir()) {
                    continue;
                }

                let file_name = entry.file_name();
                let dir_name = match file_name.to_str() {
                    Some(name) => name,
                    None => continue,
                };

                let size = match dir_name
                    .split('x')
                    .next()
                    .and_then(|s| s.parse::<u32>().ok())
                {
                    Some(size) => size,
                    None => continue,
                };

                if size < 32 {
                    continue;
                }

                let icon_path = entry.path().join("apps").join(format!("{}.png", app_id));
                if icon_path.exists() {
                    return Some(icon_path.into_os_string().into_string().unwrap());
                }
            }
        }
        Err(_) => return None,
    }
    None
}

/// Converts a `libflatpak::InstalledRef` into a `FlatpakRef` struct.
///
/// # Arguments
///
/// * `value` - The value to convert into a `FlatpakRef`.
///
/// # Returns
///
/// The converted `FlatpakRef` struct.
impl<O: glib::object::IsA<libflatpak::InstalledRef>> From<O> for FlatpakRef {
    #[must_use]
    fn from(value: O) -> Self {
        let value = value.upcast();

        Self {
            kind: value.kind().into(),
            ref_: value.format_ref_cached().unwrap().into(),
            id: value.name().unwrap().into(),
            arch: value.arch().unwrap().into(),
            branch: value.branch().unwrap().into(),
            commit: value.commit().unwrap().into(),
            origin: value.origin().unwrap().into(),
            name: value.appdata_name().map(|s| s.into()),
            version: value.appdata_version().map(|s| s.into()),
            license: value.appdata_license().map(|s| s.into()),
            summary: value.appdata_summary().map(|s| s.into()),
            oars: value.appdata_content_rating_type().map(|s| s.into()),
            ignored: false,
        }
    }
}

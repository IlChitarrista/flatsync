use crate::dbus::DaemonProxy;
use glib::Boxed;

#[derive(Boxed, Clone, Debug)]
#[boxed_type(name = "DaemonProxyBoxed")]
pub struct DaemonProxyBoxed(pub DaemonProxy<'static>);

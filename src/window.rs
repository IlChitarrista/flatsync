use crate::application::FlatsyncApplication;
use crate::authentication_page::AuthenticationPage;
use crate::glib::MainContext;
use crate::ignored_apps_dialog::IgnoredAppsDialog;
use crate::network_state::NetworkState;
use crate::power_state::PowerState;
use crate::setup::GithubLoginMessage;
use crate::setup_completed_page::SetupCompletedPage;
use adw::prelude::*;
use adw::subclass::prelude::AdwApplicationWindowImpl;
use adw::subclass::prelude::AdwWindowImpl;
use futures_util::StreamExt;
use gettextrs::gettext;
use gtk::glib::clone;
use gtk::glib::Properties;
use gtk::subclass::prelude::*;
use gtk::Align;
use gtk::{gio, glib};
use libflatsync_common::config::{APP_ID, PROFILE};
use libflatsync_common::dbus::DaemonProxy;
use libflatsync_common::providers::github::GitHubProvider;
use libflatsync_common::providers::oauth_client::OauthClientDeviceFlow;
use libflatsync_common::proxy::DaemonProxyBoxed;
use libflatsync_common::runtime::TokioRuntimeBoxed;
use libflatsync_common::state::{DaemonError, DaemonState};
use libflatsync_common::FrontendFlatpakRef;
use log::{error, warn};
use std::cell::{OnceCell, RefCell};
use std::sync::Arc;
use tokio::runtime::Runtime;

mod imp {
    use super::*;
    use std::cell::Cell;

    #[derive(Debug, Properties, gtk::CompositeTemplate)]
    #[properties(wrapper_type = super::FlatsyncApplicationWindow)]
    #[template(resource = "/app/drey/FlatSync/ui/window.ui")]
    pub struct FlatsyncApplicationWindow {
        #[template_child]
        pub view_switcher: TemplateChild<adw::ViewSwitcher>,
        #[template_child]
        pub switcher_bar: TemplateChild<adw::ViewSwitcherBar>,
        #[template_child]
        pub state_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub view_stack: TemplateChild<adw::ViewStack>,
        #[template_child]
        pub ignored_apps_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub ignored_status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub add_apps_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub ignored_apps_page: TemplateChild<adw::PreferencesPage>,
        #[template_child]
        pub ignored_apps_listbox: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub error_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub error_page_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub autosync_status: TemplateChild<adw::Banner>,
        #[template_child]
        pub welcome_status: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub sync_now_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub sync_now_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub sync_now_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub sync_now_spinner: TemplateChild<adw::Spinner>,
        pub status: Cell<DaemonState>,
        pub settings: gio::Settings,
        pub network_monitor: gio::NetworkMonitor,
        pub power_profile_monitor: gio::PowerProfileMonitor,
        pub authentication_dialog: OnceCell<adw::Dialog>,
        pub authentication_stack: OnceCell<gtk::Stack>,
        pub authentication_page: OnceCell<RefCell<AuthenticationPage>>,
        pub authentication_completed_page: OnceCell<SetupCompletedPage>,
        pub app_list: RefCell<Vec<FrontendFlatpakRef>>,
        #[property(construct_only, name = "runtime")]
        pub tokio_runtime: OnceCell<TokioRuntimeBoxed>,
        #[property(construct_only, name = "proxy")]
        pub proxy: OnceCell<DaemonProxyBoxed>,
    }

    impl Default for FlatsyncApplicationWindow {
        fn default() -> Self {
            Self {
                view_switcher: TemplateChild::default(),
                switcher_bar: TemplateChild::default(),
                state_stack: TemplateChild::default(),
                view_stack: TemplateChild::default(),
                ignored_apps_stack: TemplateChild::default(),
                ignored_status_page: TemplateChild::default(),
                add_apps_button: TemplateChild::default(),
                ignored_apps_page: TemplateChild::default(),
                ignored_apps_listbox: TemplateChild::default(),
                error_page: TemplateChild::default(),
                error_page_button: TemplateChild::default(),
                autosync_status: TemplateChild::default(),
                welcome_status: TemplateChild::default(),
                sync_now_button: TemplateChild::default(),
                sync_now_stack: TemplateChild::default(),
                sync_now_label: TemplateChild::default(),
                sync_now_spinner: TemplateChild::default(),
                status: Cell::new(DaemonState::default()),
                settings: gio::Settings::new(APP_ID),
                network_monitor: gio::NetworkMonitor::default(),
                power_profile_monitor: gio::PowerProfileMonitor::get_default(),
                authentication_dialog: OnceCell::new(),
                authentication_stack: OnceCell::new(),
                authentication_page: OnceCell::new(),
                authentication_completed_page: OnceCell::new(),
                app_list: RefCell::new(Vec::new()),
                tokio_runtime: OnceCell::new(),
                proxy: OnceCell::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FlatsyncApplicationWindow {
        const NAME: &'static str = "FlatsyncApplicationWindow";
        type Type = super::FlatsyncApplicationWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for FlatsyncApplicationWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load initial settings
            // obj.setup_settings_values();

            //TODO: Load Title and Summary dynamically from metadata
            self.welcome_status.set_title("FlatSync");
            self.welcome_status.set_description(Some(&gettext(
                "Keep your Flatpak apps synchronized between devices",
            )));
            self.welcome_status.set_icon_name(Some(APP_ID));

            obj.setup_status_tracking();
            obj.init_authentication_dialog();

            // Load latest window state
            obj.load_window_size();

            obj.load_apps();
            obj.connect_handlers();
        }
    }

    impl WidgetImpl for FlatsyncApplicationWindow {}
    impl WindowImpl for FlatsyncApplicationWindow {
        // Save window state on delete event
        fn close_request(&self) -> glib::Propagation {
            if let Err(err) = self.obj().save_window_size() {
                warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for FlatsyncApplicationWindow {}
    impl AdwWindowImpl for FlatsyncApplicationWindow {}
    impl AdwApplicationWindowImpl for FlatsyncApplicationWindow {}
}

glib::wrapper! {
    pub struct FlatsyncApplicationWindow(ObjectSubclass<imp::FlatsyncApplicationWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::Window,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl FlatsyncApplicationWindow {
    pub fn new(
        app: &FlatsyncApplication,
        tokio_runtime: Arc<Runtime>,
        proxy: DaemonProxy<'static>,
    ) -> Self {
        glib::Object::builder()
            .property("application", app)
            .property("runtime", TokioRuntimeBoxed(tokio_runtime))
            .property("proxy", DaemonProxyBoxed(proxy))
            .build()
    }

    fn proxy(&self) -> &DaemonProxy<'static> {
        &self.imp().proxy.get().unwrap().0
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let imp = self.imp();

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        imp.settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn state_changed(&self) {
        let imp = self.imp();
        let state = imp.status.get();

        let view_stack = &imp.view_stack.get();
        let error_page = &imp.error_page.get();

        self.update_sync_now_button(state, self.network_state());

        match state {
            DaemonState::Error(error) => {
                self.update_error_page(error);
                imp.state_stack.set_visible_child(error_page);
                imp.view_switcher.set_visible(false);
                imp.switcher_bar.set_visible(false);
            }
            _ => {
                imp.state_stack.set_visible_child(view_stack);
                imp.view_switcher.set_visible(true);
                imp.switcher_bar.set_visible(true);
            }
        }
    }

    fn setup_status_tracking(&self) {
        let proxy = self.proxy();
        let tokio_runtime = self.imp().tokio_runtime.get().unwrap();

        let (sender, receiver) = async_channel::bounded(1);

        tokio_runtime.0.spawn(clone!(
            #[strong]
            proxy,
            async move {
                let mut stream = proxy.receive_state_changed().await;

                while let Some(stream) = stream.next().await {
                    match stream.get().await {
                        Ok(state) => {
                            sender.send(state).await.unwrap();
                        }
                        Err(_) => {
                            error!("Failed to get new state");
                        }
                    }
                }
            }
        ));

        glib::spawn_future_local(clone!(
            #[weak(rename_to = obj)]
            self,
            async move {
                while let Ok(state) = receiver.recv().await {
                    obj.imp().status.set(state);
                    obj.state_changed();
                }
            }
        ));
    }

    fn load_window_size(&self) {
        let imp = self.imp();

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");
        let is_maximized = imp.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    fn connect_handlers(&self) {
        let imp = self.imp();

        imp.network_monitor.connect_network_changed(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_, _| {
                obj.update_autosync_status();
            }
        ));

        imp.power_profile_monitor
            .connect_power_saver_enabled_notify(clone!(
                #[weak(rename_to = obj)]
                self,
                move |_| {
                    obj.update_autosync_status();
                }
            ));

        imp.settings.connect_changed(
            Some("autosync"),
            clone!(
                #[weak(rename_to = obj)]
                self,
                move |_, _| {
                    obj.update_autosync_status();
                }
            ),
        );
        // The Setting Key only emits the `changed` signal if it has been read after the listener has been setted up
        imp.settings.get::<bool>("autosync");

        imp.sync_now_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                let ctx = MainContext::default();
                ctx.spawn_local(clone!(
                    #[weak]
                    obj,
                    async move {
                        let _ = obj.proxy().sync_now().await;
                    }
                ));
            }
        ));

        imp.add_apps_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.open_ignored_apps_dialog();
            }
        ));

        imp.error_page_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.open_authentication_dialog();
            }
        ));
    }

    fn open_ignored_apps_dialog(&self) {
        let dialog = IgnoredAppsDialog::new(self.proxy().clone());

        dialog.connect_closed(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.load_apps();
            }
        ));

        dialog.present(Some(self));
    }

    fn setup_github_login(&self) {
        let imp = self.imp();
        let github = GitHubProvider::new();

        let runtime = imp.tokio_runtime.get().unwrap();
        let (sender, receiver) = async_channel::bounded(2);

        runtime.0.spawn(clone!(
            #[strong]
            sender,
            async move {
                loop {
                    if let Ok(device_auth_res) = github.device_code().await {
                        sender
                            .send(GithubLoginMessage::UserCode(
                                device_auth_res.user_code().secret().to_string(),
                            ))
                            .await
                            .unwrap();

                        if let Ok(token_pair) = github.register_device(device_auth_res).await {
                            sender
                                .send(GithubLoginMessage::Secret(
                                    serde_json::to_string(&token_pair).unwrap(),
                                ))
                                .await
                                .unwrap();
                            break;
                        }
                    }
                }
            }
        ));

        glib::spawn_future_local(clone!(
            #[weak(rename_to = obj)]
            self,
            async move {
                while let Ok(msg) = receiver.recv().await {
                    match msg {
                        GithubLoginMessage::UserCode(code) => obj
                            .imp()
                            .authentication_page
                            .get()
                            .unwrap()
                            .borrow_mut()
                            .set_token_row_title(&code),
                        GithubLoginMessage::Secret(secret) => obj.complete_login(&secret).await,
                    }
                }
            }
        ));
    }

    async fn complete_login(&self, secret: &str) {
        let imp = self.imp();
        let proxy = self.proxy();

        proxy.set_gist_secret(secret).await.unwrap();

        imp.authentication_stack
            .get()
            .unwrap()
            .set_visible_child(imp.authentication_completed_page.get().unwrap());
    }

    fn open_authentication_dialog(&self) {
        self.setup_github_login();

        self.imp()
            .authentication_dialog
            .get()
            .unwrap()
            .present(Some(self));
    }

    fn init_authentication_dialog(&self) {
        let imp = self.imp();

        let authentication_dialog = adw::Dialog::builder().width_request(360).build();

        let toast_overlay = adw::ToastOverlay::new();
        let authentication_stack = gtk::Stack::builder()
            .hhomogeneous(true)
            .transition_type(gtk::StackTransitionType::Crossfade)
            .build();

        let authentication_page = AuthenticationPage::new(true, &toast_overlay);
        let authentication_completed_page = SetupCompletedPage::new(true);

        let obj = self.clone();
        let obj2 = self.clone();

        authentication_page.connect_local("open-uri", false, move |links| {
            let uri_launcher = gtk::UriLauncher::new(links[1].get().unwrap());
            uri_launcher.launch(Some(&obj), Some(&gio::Cancellable::new()), |_| {});
            None
        });

        authentication_completed_page.connect_local("continue-button-pressed", false, move |_| {
            obj2.imp().authentication_dialog.get().unwrap().close();
            obj2.imp()
                .authentication_stack
                .get()
                .unwrap()
                .set_visible_child(
                    &obj2
                        .imp()
                        .authentication_page
                        .get()
                        .unwrap()
                        .borrow()
                        .clone(),
                );
            None
        });

        authentication_stack.add_child(&authentication_page);
        authentication_stack.add_child(&authentication_completed_page);

        toast_overlay.set_child(Some(&authentication_stack));
        authentication_dialog.set_child(Some(&toast_overlay));

        imp.authentication_dialog
            .set(authentication_dialog)
            .unwrap();
        imp.authentication_page
            .set(RefCell::new(authentication_page))
            .unwrap();
        imp.authentication_stack.set(authentication_stack).unwrap();
        imp.authentication_completed_page
            .set(authentication_completed_page)
            .unwrap();
    }

    fn network_state(&self) -> NetworkState {
        let imp = self.imp();

        let network_is_metered = imp.network_monitor.is_network_metered();
        let network_is_available = imp.network_monitor.is_network_available();

        match (network_is_metered, network_is_available) {
            (_, false) => NetworkState::NoNetwork,
            (false, true) => NetworkState::Ok,
            (true, _) => NetworkState::NetworkMetered,
        }
    }

    fn power_state(&self) -> PowerState {
        let imp = self.imp();

        match imp.power_profile_monitor.is_power_saver_enabled() {
            true => PowerState::PowerSaver,
            false => PowerState::Ok,
        }
    }

    fn update_sync_now_button(
        &self,
        daemon_state: DaemonState,
        network_monitor_state: NetworkState,
    ) {
        let imp = self.imp();

        let label = imp.sync_now_label.get();
        let spinner = imp.sync_now_spinner.get();

        match (daemon_state, network_monitor_state) {
            (_, NetworkState::NoNetwork) => {
                imp.sync_now_button.set_sensitive(false);
                imp.sync_now_stack.set_visible_child(&label);
            }
            (DaemonState::Idle, _) => {
                imp.sync_now_button.set_sensitive(true);
                imp.sync_now_stack.set_visible_child(&label);
            }
            (DaemonState::Syncing, _) => {
                imp.sync_now_button.set_sensitive(false);
                imp.sync_now_stack.set_visible_child(&spinner);
            }
            (DaemonState::Error(_), _) => {
                imp.sync_now_button.set_sensitive(false);
                imp.sync_now_stack.set_visible_child(&label);
            }
        }
    }

    fn update_error_page(&self, error: DaemonError) {
        match error {
            DaemonError::InvalidToken => {
                self.imp().error_page.set_title(&gettext("Login Expired"));
                self.imp()
                    .error_page
                    .set_description(Some(&gettext("Reauthenticate to restore synchronization")));
                self.imp()
                    .error_page_button
                    .set_label(&gettext("Reauthenticate"));
            }
        }
    }

    fn update_autosync_status(&self) {
        let imp = self.imp();

        let network_monitor_state = self.network_state();
        let power_profiles_monitor_state = self.power_state();
        let is_autosync_enabled = imp.settings.get::<bool>("autosync");

        self.update_sync_now_button(imp.status.get(), network_monitor_state);

        let title: Option<String> = match (
            network_monitor_state,
            power_profiles_monitor_state,
            is_autosync_enabled,
        ) {
            (NetworkState::Ok, PowerState::Ok, _) => None,
            (NetworkState::Ok, PowerState::PowerSaver, true) => {
                Some(gettext("Autosync disabled: power saver is on"))
            }
            (NetworkState::NoNetwork, PowerState::Ok, true) => {
                Some(gettext("Autosync disabled: system is offline"))
            }
            (NetworkState::NoNetwork, PowerState::PowerSaver, true) => Some(gettext(
                "Autosync disabled: system is offline, power saver is on",
            )),
            (NetworkState::NoNetwork, _, false) => Some(gettext("System is offline")),
            (NetworkState::NetworkMetered, PowerState::Ok, true) => {
                Some(gettext("Autosync disabled: network is metered"))
            }
            (NetworkState::NetworkMetered, PowerState::PowerSaver, true) => Some(gettext(
                "Autosync disabled: network is metered, power saver is on",
            )),
            (_, _, _) => None,
        };

        if let Some(title) = title {
            imp.autosync_status.set_title(&title);
            imp.autosync_status.set_revealed(true);
        } else {
            imp.autosync_status.set_revealed(false);
        }
    }

    fn load_apps(&self) {
        let ctx = MainContext::default();

        ctx.spawn_local(clone!(
            #[weak(rename_to = obj)]
            self,
            async move {
                let proxy = &obj.imp().proxy.get().unwrap().0;

                let ignored_apps = proxy.ignored_apps().await.unwrap();
                let apps = proxy.app_list().await.unwrap();
                {
                    let mut app_list = obj.imp().app_list.borrow_mut();
                    *app_list = apps
                        .into_iter()
                        .filter(|app| ignored_apps.contains(&app.id))
                        .collect();
                }

                obj.process_apps();
            }
        ));
    }

    fn process_apps(&self) {
        let imp = self.imp();

        imp.ignored_apps_listbox.remove_all();

        let app_list = imp.app_list.borrow();

        let ignored_status_page = imp.ignored_status_page.get();
        let ignored_apps_page = imp.ignored_apps_page.get();

        if app_list.len() > 0 {
            imp.ignored_apps_stack
                .get()
                .set_visible_child(&ignored_apps_page);
        } else {
            imp.ignored_apps_stack
                .get()
                .set_visible_child(&ignored_status_page);
        }

        for ref_ in app_list.iter() {
            self.process_app(ref_);
        }

        let button_row = adw::ButtonRow::builder()
            .title(gettext("_Add Apps"))
            .use_underline(true)
            .start_icon_name("plus-large-symbolic")
            .build();

        button_row.connect_activated(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.open_ignored_apps_dialog();
            }
        ));

        imp.ignored_apps_listbox.append(&button_row);
    }

    fn process_app(&self, ref_: &FrontendFlatpakRef) {
        if !ref_.ignored {
            return;
        }

        let imp = self.imp();

        let button = gtk::Button::builder()
            .icon_name("cross-large-circle-outline-symbolic")
            .valign(Align::Center)
            .focus_on_click(false)
            .tooltip_text(gettext("Remove"))
            .build();
        button.add_css_class("flat");

        let id = ref_.id.clone();
        let row = adw::ActionRow::builder()
            .title(ref_.name.clone())
            .selectable(false)
            .width_request(300)
            .build();

        let image = gtk::Image::from_file(&ref_.icon);
        image.set_pixel_size(32);

        row.add_prefix(&image);
        row.add_suffix(&button);
        button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            #[weak]
            row,
            move |_| {
                let imp = obj.imp();
                let ctx = MainContext::default();

                imp.ignored_apps_listbox.remove(&row);
                if imp.app_list.borrow().len() == 1 {
                    imp.ignored_apps_stack
                        .set_visible_child(&imp.ignored_status_page.get());
                };

                ctx.spawn_local(clone!(
                    #[weak]
                    obj,
                    #[strong]
                    id,
                    async move {
                        obj.imp().app_list.borrow_mut().retain(|ref_| ref_.id != id);
                        match obj.proxy().remove_ignored_app(id).await.unwrap() {
                            true => (),
                            false => panic!("The provided App ID doesn't match an Ignored App"),
                        };
                    }
                ));
            }
        ));

        imp.ignored_apps_listbox.append(&row);
    }

    // fn setup_settings_values(&self) {
    //     let imp = self.imp();
    // }
}

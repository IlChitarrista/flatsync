use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use gtk::{
    gio,
    glib::subclass::Signal,
    glib::{self, clone, Properties},
};
use std::sync::OnceLock;

mod imp {
    use super::*;
    use std::cell::OnceCell;

    #[derive(Debug, Properties, gtk::CompositeTemplate, Default)]
    #[properties(wrapper_type = super::SetupCompletedPage)]
    #[template(resource = "/app/drey/FlatSync/ui/setup_completed_page.ui")]
    pub struct SetupCompletedPage {
        #[property(construct_only, name = "dialog")]
        pub setup_completed_dialog: OnceCell<bool>,
        #[template_child]
        pub setup_completed_status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub complete_setup_button: TemplateChild<gtk::Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SetupCompletedPage {
        const NAME: &'static str = "SetupCompletedPage";
        type Type = super::SetupCompletedPage;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for SetupCompletedPage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            obj.connect_handlers();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| vec![Signal::builder("continue-button-pressed").build()])
        }
    }
    impl WidgetImpl for SetupCompletedPage {}
    impl BinImpl for SetupCompletedPage {}
}

glib::wrapper! {
    pub struct SetupCompletedPage(ObjectSubclass<imp::SetupCompletedPage>)
        @extends gtk::Widget, adw::Bin,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl SetupCompletedPage {
    pub fn new(setup_completed_dialog: bool) -> Self {
        glib::Object::builder()
            .property("dialog", setup_completed_dialog)
            .build()
    }

    fn connect_handlers(&self) {
        let imp = self.imp();

        match imp.setup_completed_dialog.get().unwrap() {
            true => {
                imp.setup_completed_status_page
                    .set_title(&gettext("Login Completed"));
            }
            false => {
                imp.setup_completed_status_page
                    .set_title(&gettext("Setup Completed"));
            }
        }

        imp.complete_setup_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.emit_continue_button_pressed();
            }
        ));
    }

    fn emit_continue_button_pressed(&self) {
        self.emit_by_name::<()>("continue-button-pressed", &[]);
    }
}

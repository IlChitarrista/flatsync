use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use gtk::gdk::Display;
use gtk::{
    gio,
    glib::subclass::Signal,
    glib::{self, clone, Properties},
};
use std::sync::OnceLock;

mod imp {
    use super::*;
    use std::cell::OnceCell;

    #[derive(Debug, Properties, gtk::CompositeTemplate, Default)]
    #[properties(wrapper_type = super::AuthenticationPage)]
    #[template(resource = "/app/drey/FlatSync/ui/authentication_page.ui")]
    pub struct AuthenticationPage {
        #[property(construct_only, name = "dialog")]
        pub authentication_dialog: OnceCell<bool>,
        #[property(construct_only, name = "overlay")]
        pub toast_overlay: OnceCell<adw::ToastOverlay>,
        #[template_child]
        pub authentication_status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub token_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub token_copy_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub login_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub login_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub waiting_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub login_button_done: TemplateChild<gtk::Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AuthenticationPage {
        const NAME: &'static str = "AuthenticationPage";
        type Type = super::AuthenticationPage;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for AuthenticationPage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            obj.connect_handlers();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| {
                vec![
                    Signal::builder("login-done").build(),
                    Signal::builder("open-uri")
                        .param_types([str::static_type()])
                        .build(),
                ]
            })
        }
    }
    impl WidgetImpl for AuthenticationPage {}
    impl BinImpl for AuthenticationPage {}
}

glib::wrapper! {
    pub struct AuthenticationPage(ObjectSubclass<imp::AuthenticationPage>)
        @extends gtk::Widget, adw::Bin,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl AuthenticationPage {
    pub fn new(authentication_dialog: bool, overlay: &adw::ToastOverlay) -> Self {
        glib::Object::builder()
            .property("dialog", authentication_dialog)
            .property("overlay", overlay)
            .build()
    }

    pub fn complete_login(&self) {
        self.imp()
            .login_stack
            .set_visible_child(&self.imp().login_button_done.get());
    }

    pub fn token_row_title(&self) -> String {
        self.imp().token_row.title().into()
    }

    pub fn set_token_row_title(&mut self, title: &str) {
        self.imp().token_row.set_title(title);
    }

    fn connect_handlers(&self) {
        let imp = self.imp();

        match imp.authentication_dialog.get().unwrap() {
            true => {
                imp.authentication_status_page.get().set_description(Some(&gettext("Enter the following code in Github's Device Activation to reauthenticate")))
            },
            false => {
                imp.authentication_status_page.get().set_description(Some(&gettext("Finally, enter the following code in Github's Device Activation to complete the setup")))
            }
        }

        imp.token_copy_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                let id = obj.imp().token_row.title();
                Display::default().unwrap().clipboard().set_text(&id);
                obj.imp()
                    .toast_overlay
                    .get()
                    .unwrap()
                    .add_toast(adw::Toast::new(&gettext("Copied to Clipboard")));
            }
        ));

        imp.login_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.emit_open_uri("https://github.com/login/device");
            }
        ));
        imp.waiting_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.emit_open_uri("https://github.com/login/device");
            }
        ));

        imp.login_button_done.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.emit_login_done();
            }
        ));
    }

    fn emit_open_uri(&self, link: &str) {
        self.imp()
            .login_stack
            .set_visible_child(&self.imp().waiting_button.get());
        self.emit_by_name::<()>("open-uri", &[&link]);
    }

    fn emit_login_done(&self) {
        self.emit_by_name::<()>("login-done", &[]);
    }
}

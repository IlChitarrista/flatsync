use crate::glib::ControlFlow;
use crate::glib::MainContext;
use crate::glib::Properties;
use adw::prelude::*;
use adw::subclass::prelude::AdwDialogImpl;
use glib::clone;
use glib::source::idle_add_local;
use gtk::subclass::prelude::*;
use gtk::Align;
use gtk::{gio, glib};
use libflatsync_common::dbus::DaemonProxy;
use libflatsync_common::proxy::DaemonProxyBoxed;
use libflatsync_common::FrontendFlatpakRef;
use std::cell::{OnceCell, RefCell};
use std::rc::Rc;
use std::vec::IntoIter;

use fuzzy_matcher::skim::SkimMatcherV2;
use fuzzy_matcher::FuzzyMatcher;

mod imp {
    use super::*;

    #[derive(Properties, Default, gtk::CompositeTemplate)]
    #[properties(wrapper_type = super::IgnoredAppsDialog)]
    #[template(resource = "/app/drey/FlatSync/ui/ignored_apps_dialog.ui")]
    pub struct IgnoredAppsDialog {
        #[template_child]
        pub add_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub search_entry: TemplateChild<gtk::SearchEntry>,
        #[template_child]
        pub search_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub no_apps_found: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub preferences_page: TemplateChild<adw::PreferencesPage>,
        #[template_child]
        pub listbox: TemplateChild<gtk::ListBox>,
        pub app_list: RefCell<Vec<FrontendFlatpakRef>>,
        pub selected_apps: RefCell<Vec<String>>,
        #[property(construct_only, name = "proxy")]
        pub proxy: OnceCell<DaemonProxyBoxed>,
        pub matcher: SkimMatcherV2,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for IgnoredAppsDialog {
        const NAME: &'static str = "IgnoredAppsDialog";
        type Type = super::IgnoredAppsDialog;
        type ParentType = adw::Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for IgnoredAppsDialog {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            obj.connect_handlers();

            obj.load_apps();
        }
    }

    impl WidgetImpl for IgnoredAppsDialog {}
    impl WindowImpl for IgnoredAppsDialog {}
    impl AdwDialogImpl for IgnoredAppsDialog {}
}

glib::wrapper! {
    pub struct IgnoredAppsDialog(ObjectSubclass<imp::IgnoredAppsDialog>)
        @extends gtk::Widget, adw::Dialog,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl IgnoredAppsDialog {
    pub fn new(proxy: DaemonProxy<'static>) -> Self {
        glib::Object::builder()
            .property("proxy", DaemonProxyBoxed(proxy))
            .build()
    }

    fn connect_handlers(&self) {
        let imp = self.imp();

        imp.add_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                let ctx = MainContext::default();

                ctx.spawn_local(clone!(
                    #[weak]
                    obj,
                    async move {
                        let proxy = &obj.imp().proxy.get().unwrap().0;
                        let selected_apps = obj.imp().selected_apps.borrow().clone();
                        match proxy.add_ignored_apps(selected_apps).await.unwrap() {
                            true => (),
                            false => panic!(
                                "Some of the provided App IDs are invalid, ID logged by Daemon"
                            ),
                        };
                        obj.close();
                    }
                ));
            }
        ));

        self.set_focus(Some(&imp.search_entry.get()));
        imp.search_entry.connect_search_changed(clone!(
            #[weak(rename_to = obj)]
            self,
            move |entry| {
                obj.filter_apps(&entry.text());
            }
        ));
    }

    fn load_apps(&self) {
        let ctx = MainContext::default();

        ctx.spawn_local(clone!(
            #[weak(rename_to = obj)]
            self,
            async move {
                let proxy = &obj.imp().proxy.get().unwrap().0;

                let apps = proxy.app_list().await.unwrap();
                {
                    let mut app_list = obj.imp().app_list.borrow_mut();
                    *app_list = apps;
                }

                obj.process_apps();
            }
        ));
    }

    fn process_apps(&self) {
        let imp = self.imp();
        let app_list = imp.app_list.borrow().clone();
        let no_apps_found = imp.no_apps_found.get();
        let preferences_page = imp.preferences_page.get();

        if app_list.is_empty() {
            imp.search_stack.get().set_visible_child(&no_apps_found);
            return;
        } else {
            imp.search_stack.get().set_visible_child(&preferences_page);
        }

        let iter_state = Rc::new(RefCell::new(app_list.into_iter()));
        let first_iter = Rc::new(RefCell::new(true));

        idle_add_local(clone!(
            #[weak(rename_to=obj)]
            self,
            #[upgrade_or]
            ControlFlow::Break,
            move || obj.process_next_app(&iter_state, &first_iter, 0, false)
        ));
    }

    fn process_next_app(
        &self,
        iter_state: &Rc<RefCell<IntoIter<FrontendFlatpakRef>>>,
        first_iter: &Rc<RefCell<bool>>,
        chunk_size: u8,
        clean_listbox: bool,
    ) -> ControlFlow {
        let mut iter = iter_state.borrow_mut();
        let is_first_iteration = *first_iter.borrow();

        if is_first_iteration {
            if clean_listbox {
                self.imp().listbox.remove_all();
            }
            first_iter.replace(false);
        }

        for _ in 0..=chunk_size {
            match iter.next() {
                Some(ref_) => self.process_app(&ref_),
                None => return ControlFlow::Break,
            }
        }

        ControlFlow::Continue
    }

    fn process_app(&self, ref_: &FrontendFlatpakRef) {
        if ref_.ignored {
            return;
        }

        let imp = self.imp();
        let row = self.create_app_row(ref_);

        imp.listbox.append(&row);
        imp.search_stack
            .set_visible_child(&imp.preferences_page.get());
    }

    fn filter_apps(&self, search_text: &str) {
        let imp = self.imp();
        let matcher = &imp.matcher;

        let no_apps_found = imp.no_apps_found.get();
        let preferences_page = imp.preferences_page.get();

        let app_list_iter = imp.app_list.borrow().clone().into_iter();
        let filtered_app_list: Vec<FrontendFlatpakRef> = app_list_iter
            .filter(|ref_| matcher.fuzzy_match(&ref_.name, search_text).is_some())
            .collect();

        if filtered_app_list.is_empty() {
            imp.search_stack.get().set_visible_child(&no_apps_found);
            imp.listbox.remove_all();
            return;
        } else {
            imp.search_stack.get().set_visible_child(&preferences_page);
        }

        let iter_state = Rc::new(RefCell::new(filtered_app_list.into_iter()));
        let first_iter = Rc::new(RefCell::new(true));

        idle_add_local(clone!(
            #[weak(rename_to=obj)]
            self,
            #[upgrade_or]
            ControlFlow::Break,
            move || obj.process_next_app(&iter_state, &first_iter, 16, true)
        ));
    }

    fn create_app_row(&self, ref_: &FrontendFlatpakRef) -> adw::ActionRow {
        let button = gtk::CheckButton::builder().valign(Align::Center).build();
        button.add_css_class("selection-mode");

        let id = ref_.id.clone();
        let row = adw::ActionRow::builder()
            .title(ref_.name.clone())
            .selectable(false)
            .build();

        let image = gtk::Image::from_file(&ref_.icon);
        image.set_pixel_size(32);

        row.add_prefix(&image);
        row.add_suffix(&button);
        button.connect_toggled(glib::clone!(
            #[weak(rename_to = obj)]
            self,
            move |button| {
                if button.is_active() {
                    obj.imp().selected_apps.borrow_mut().push(id.to_string());
                } else {
                    obj.imp()
                        .selected_apps
                        .borrow_mut()
                        .retain(|element| *element != id);
                }
            }
        ));

        row
    }
}

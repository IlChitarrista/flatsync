use ashpd::desktop::notification::Priority;
use gettextrs::gettext;
use libflatsync_common::state::{DaemonError, DaemonWarning};

pub enum ToNotify {
    Warning(DaemonWarning),
    Error(DaemonError),
}

pub struct NotificationContent {
    pub title: String,
    pub subtitle: String,
    pub priority: Priority,
}

impl ToNotify {
    pub fn notification_content(&self) -> NotificationContent {
        match self {
            ToNotify::Warning(warning) => match warning {
                DaemonWarning::SyncTimedOut => NotificationContent {
                    title: gettext("Synchronization Failed"),
                    subtitle: gettext("Connection timed out, is the system online?"),
                    priority: Priority::Normal,
                },
            },
            ToNotify::Error(error) => match error {
                DaemonError::InvalidToken => NotificationContent {
                    title: gettext("Login Expired"),
                    subtitle: gettext("Open the app to reauthenticate"),
                    priority: Priority::High,
                },
            },
        }
    }
}
